package controller;

import java.util.ArrayList;

import model.data_structures.IntegersBag;
import model.logic.IntegersBagOperations;

public class Controller {

	private static IntegersBagOperations model = new IntegersBagOperations();
	
	
	public static IntegersBag createBag(ArrayList<Integer> values){
         return new IntegersBag(values);		
	}
	
	
	public static double getMean(IntegersBag bag){
		return model.computeMean(bag);
	}
	
	public static double getMax(IntegersBag bag){
		return model.getMax(bag);
	}
	
	public static double getMin(IntegersBag bag)
	{
		return model.getMin(bag);
	}
	
	public static double computeAverage(IntegersBag bag)
	{
		return model.computeAverage(bag);
	}
	
	public static double getQuantityEvenNumbers(IntegersBag bag)
	{
		return model.getQuantityEvenNumbers(bag);
	}
	
}
