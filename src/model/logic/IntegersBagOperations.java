package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	public int getMin(IntegersBag bag)
	{
		int min = Integer.MIN_VALUE;
		int value;
		if (bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				value = iter.next();
				if(value < min)
				{
					min = value;
				}
			}
		}
		return min;
	}
	
	
	public double computeAverage(IntegersBag bag)
	{
		double average = 0;
		int total = 0;
		int length = 0;
		if (bag != null)
		 {
			Iterator<Integer> iter = bag.getIterator();
			while (iter.hasNext())
			{
				total += iter.next().intValue();
				length++;
			}
		}
		average = total / length;
		return average;
	}
	
	
	public int getQuantityEvenNumbers( IntegersBag bag)
	{
		int evenNumbers = 0;
		if (bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while ( iter.hasNext())
			{
				if((iter.next()%2) == 0)
				{
					evenNumbers++;
				}
			}
		}
		return evenNumbers;
	}
	
	
}
